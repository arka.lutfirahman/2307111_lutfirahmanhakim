<?php
// Start or resume the session
session_start();
// get cashflow soal 1
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (!isset($_SESSION['outcomes'])) {
        $_SESSION['incomes'] = array(
            array(
                'cashflow_id' => 1,
                'cashflow_period' => '2022-01',
                'cashflow_type' => 'Cash',
                'cashflow_name' => 'Cash Euy',
                'cashflow_amount' => 10000,
            ),
            array(
                'cashflow_id' => 2,
                'cashflow_period' => '2022-02',
                'cashflow_type' => 'Cash',
                'cashflow_name' => 'Cash Euy',
                'cashflow_amount' => 10000,
            ),
        );
    }

    if (!isset($_SESSION['outcomes'])) {
        $_SESSION['outcomes'] = array(
            array(
                'cashflow_id' => 1,
                'cashflow_period' => '2022-03',
                'cashflow_type' => 'Cash',
                'cashflow_name' => 'Cash Euy',
                'cashflow_amount' => 10000,
            ),
        );
    }

    // Function to calculate the total amount for a given array of cashflows
    function calculateTotalAmount($cashflows) {
        $totalAmount = 0;
        foreach ($cashflows as $cashflow) {
            $totalAmount += $cashflow['cashflow_amount'];
        }
        return $totalAmount;
    }

    // Create XML response
    $responseXml = new SimpleXMLElement('<response/>');
    $responseXml->addChild('status', 'true');
    $responseXml->addChild('message', 'Get Cashflow successfully');

    // Add income data
    $incomesXml = $responseXml->addChild('data')->addChild('incomes');
    foreach ($_SESSION['incomes'] as $income) {
        $incomeXml = $incomesXml->addChild('income');
        foreach ($income as $key => $value) {
            $incomeXml->addChild($key, $value);
        }
    }

    // Add outcome data
    $outcomesXml = $responseXml->data->addChild('outcomes');
    foreach ($_SESSION['outcomes'] as $outcome) {
        $outcomeXml = $outcomesXml->addChild('outcome');
        foreach ($outcome as $key => $value) {
            $outcomeXml->addChild($key, $value);
        }
    }

    // Calculate and add total amounts
    $responseXml->data->addChild('income_amount_total', calculateTotalAmount($_SESSION['incomes']));
    $responseXml->data->addChild('outcome_amount_total', calculateTotalAmount($_SESSION['outcomes']));

    // Respond with XML
    echo $responseXml->asXML();
}

// end of get cashflow soal 1


// Insert cashflow soal 2
// Check if the request is a POST request
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Read the raw POST data
    $postData = file_get_contents("php://input");

    // Parse XML data
    $requestXml = simplexml_load_string($postData);

    // Check if the XML structure is as expected
    if (isset($requestXml->incomes->income) && isset($requestXml->outcomes->outcome)) {
        // Extract income data from XML
        $incomeData = array(
            'cashflow_id' => count($_SESSION['incomes']) + 1,
            'cashflow_period' => (string)$requestXml->incomes->income->cashflow_period,
            'cashflow_type' => (string)$requestXml->incomes->income->cashflow_type,
            'cashflow_name' => (string)$requestXml->incomes->income->cashflow_name,
            'cashflow_amount' => (int)$requestXml->incomes->income->cashflow_amount,
        );

        // Validate cashflow_period format (YYYY-MM)
        if (!isValidCashflowPeriod($incomeData['cashflow_period'])) {
            header("HTTP/1.1 400 Bad Request");
            echo "Invalid Income cashflow_period format. Accepted format is YYYY-MM.";
            exit();
        }

        // Add the income to $_SESSION['incomes']
        if (!isset($_SESSION['incomes'])) {
            $_SESSION['incomes'] = array();
        }
        $_SESSION['incomes'][] = $incomeData;


        // Extract outcome data from XML
        $outcomeData = array(
            'cashflow_id' => count($_SESSION['outcomes']) + 1,
            'cashflow_period' => (string)$requestXml->outcomes->outcome->cashflow_period,
            'cashflow_type' => (string)$requestXml->outcomes->outcome->cashflow_type,
            'cashflow_name' => (string)$requestXml->outcomes->outcome->cashflow_name,
            'cashflow_amount' => (int)$requestXml->outcomes->outcome->cashflow_amount,
        );

        // Validate cashflow_period format (YYYY-MM)
        if (!isValidCashflowPeriod($incomeData['cashflow_period'])) {
            header("HTTP/1.1 400 Bad Request");
            echo "Invalid Outcome cashflow_period format. Accepted format is YYYY-MM.";
            exit();
        }

        // Add the income to $_SESSION['incomes']
        if (!isset($_SESSION['outcomes'])) {
            $_SESSION['outcomes'] = array();
        }
        $_SESSION['outcomes'][] = $outcomeData;

        // Respond with success message or updated data
        $responseXml = new SimpleXMLElement('<response/>');
        $responseXml->addChild('status', 'true');
        $responseXml->addChild('message', 'Income Or Outcome added successfully');
        echo $responseXml->asXML();
    } else {
        // Respond with an error if the XML structure is not as expected
        header("HTTP/1.1 400 Bad Request");
        echo "Invalid XML structure";
    }
} 


//End of Insert cashflow soal 2


// update cashflow soal 3
// Check if the request is a POST request
if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    // Read the raw POST data
    $postData = file_get_contents("php://input");

    // Parse XML data
    $requestXml = simplexml_load_string($postData);

    // Check if the XML structure is as expected
    if (isset($requestXml->incomes->income) && isset($requestXml->outcomes->outcome)) {
        // Extract income data from XML
        $incomeData = array(
            'cashflow_id' => (string)$requestXml->incomes->income->cashflow_id,
            'cashflow_period' => (string)$requestXml->incomes->income->cashflow_period,
            'cashflow_type' => (string)$requestXml->incomes->income->cashflow_type,
            'cashflow_name' => (string)$requestXml->incomes->income->cashflow_name,
            'cashflow_amount' => (int)$requestXml->incomes->income->cashflow_amount,
        );

        // Validate cashflow_period format (YYYY-MM)
        if (!isValidCashflowPeriod($incomeData['cashflow_period'])) {
            header("HTTP/1.1 400 Bad Request");
            echo "Invalid Income cashflow_period format. Accepted format is YYYY-MM.";
            exit();
        }

        // Check if the income entry with the same cashflow_id already exists
        $existingIndex = findIncomeIndex($_SESSION['incomes'], $incomeData['cashflow_id']);

        if ($existingIndex !== false) {
            // Update the existing entry
            $_SESSION['incomes'][$existingIndex] = $incomeData;
            $responseMessage = 'Income Or Outcome updated successfully';
        } else {
            header("HTTP/1.1 400 Bad Request");
            echo "Income Data not found.";
            exit();
        }


        // Extract income data from XML
        $outcomeData = array(
            'cashflow_id' => (string)$requestXml->outcomes->outcome->cashflow_id,
            'cashflow_period' => (string)$requestXml->outcomes->outcome->cashflow_period,
            'cashflow_type' => (string)$requestXml->outcomes->outcome->cashflow_type,
            'cashflow_name' => (string)$requestXml->outcomes->outcome->cashflow_name,
            'cashflow_amount' => (int)$requestXml->outcomes->outcome->cashflow_amount,
        );

        // Validate cashflow_period format (YYYY-MM)
        if (!isValidCashflowPeriod($outcomeData['cashflow_period'])) {
            header("HTTP/1.1 400 Bad Request");
            echo "Invalid outcome cashflow_period format. Accepted format is YYYY-MM.";
            exit();
        }

        // Check if the income entry with the same cashflow_id already exists
        $existingIndex = findOutcomeIndex($_SESSION['outcomes'], $outcomeData['cashflow_id']);

        if ($existingIndex !== false) {
            // Update the existing entry
            $_SESSION['outcomes'][$existingIndex] = $outcomeData;
            $responseMessage = 'Income Or Outcome updated successfully';
        } else {
            header("HTTP/1.1 400 Bad Request");
            echo " Outcome Data not found.";
            exit();
        }


        // Respond with success message or updated data
        $responseXml = new SimpleXMLElement('<response/>');
        $responseXml->addChild('status', 'true');
        $responseXml->addChild('message', $responseMessage);
        echo $responseXml->asXML();
    } else {
        // Respond with an error if the XML structure is not as expected
        header("HTTP/1.1 400 Bad Request");
        echo "Invalid XML structure";
    }
} 


// Function to validate the cashflow_period format (YYYY-MM)
function isValidCashflowPeriod($cashflowPeriod)
{
    return (bool)preg_match('/^\d{4}-\d{2}$/', $cashflowPeriod);
}
// end of update cashflow soal 3

// delete cashflow soal 5
if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    // Read the raw DELETE request data
    $deleteData = file_get_contents("php://input");
    $deleteXml = simplexml_load_string($deleteData);

    // Validate XML structure
    if (isset($deleteXml->cashflow_id)) {
        // Extract cashflow_id from XML
        $cashflowIdToDelete = (string)$deleteXml->cashflow_id;

        // Check if the income entry with the specified cashflow_id exists
        $existingIndex = findIncomeIndex($_SESSION['incomes'], $cashflowIdToDelete);

        if ($existingIndex !== false) {
            // Delete the income entry
            unset($_SESSION['incomes'][$existingIndex]);
            $_SESSION['incomes'] = array_values($_SESSION['incomes']); // Reindex the array

            // Respond with success message
            $responseXml = new SimpleXMLElement('<response/>');
            $responseXml->addChild('status', 'true');
            $responseXml->addChild('message', 'Income deleted successfully');
            echo $responseXml->asXML();
            exit();
        } 

        // Check if the income entry with the specified cashflow_id exists
        if($existingIndex == false) {
            $existingIndex = findOutcomeIndex($_SESSION['outcomes'], $cashflowIdToDelete);
            if ($existingIndex !== false) {
                // Delete the income entry
                unset($_SESSION['outcomes'][$existingIndex]);
                $_SESSION['outcomes'] = array_values($_SESSION['outcomes']); // Reindex the array

                // Respond with success message
                $responseXml = new SimpleXMLElement('<response/>');
                $responseXml->addChild('status', 'true');
                $responseXml->addChild('message', 'Outcome deleted successfully');
                echo $responseXml->asXML();
            } else {
                header("HTTP/1.1 400 Bad Request");
                echo "Data not found.";
                exit();
            }
        }

    } else {
        // Respond with an error if the XML structure is not as expected
        header("HTTP/1.1 400 Bad Request");
        echo "Invalid XML structure. Expected <cashflow_id> in the request body.";
    }
} 

// Function to find the index of an income entry with a given cashflow_id
function findIncomeIndex($incomes, $cashflowId)
{
    foreach ($incomes as $index => $income) {
        if ($income['cashflow_id'] == $cashflowId) {
            return $index;
        }
    }
    return false; // Not found
}


function findOutcomeIndex($outcomes, $cashflowId)
{
    foreach ($outcomes as $index => $outcome) {
        if ($outcome['cashflow_id'] == $cashflowId) {
            return $index;
        }
    }
    return false; // Not found
}

// end of soal 5
?>
