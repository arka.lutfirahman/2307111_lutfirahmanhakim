<?php
header("Content-Type: application/xml");

// Handling POST request
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get the raw POST data
    $postData = file_get_contents("php://input");

    // Decode XML data
    $xml = simplexml_load_string($postData);

    // Check if the 'number' elements exist     in the request payload
    if ($xml && $xml->count() > 0) {
        // Convert XML 'number' elements to array
        $numbers = [];
        foreach ($xml->number as $number) {
            $numbers[] = intval($number);
        }

        // Reverse the array of numbers
        $reversedNumbers = array_reverse($numbers);

        // Response data
        $responseXml = new SimpleXMLElement('<response/>');
        foreach ($reversedNumbers as $number) {
            $responseXml->addChild('number', $number);
        }

        // Respond with XML
        echo $responseXml->asXML();
    } else {
        // Invalid request payload
        http_response_code(400); // Bad Request
        echo '<response><message>Invalid request payload.</message></response>';
    }
} else {
    // Respond to non-POST requests
    http_response_code(405); // Method Not Allowed
    echo '<response><message>Method not allowed.</message></response>';
}
?>
